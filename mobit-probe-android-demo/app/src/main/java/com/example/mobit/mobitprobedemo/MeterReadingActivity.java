package com.example.mobit.mobitprobedemo;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ListView;
import android.widget.TextView;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.widget.ArrayAdapter;

import com.example.mobit.mobitprobelibrary.MobitProbe;

public class MeterReadingActivity extends Activity {
    private Thread mReadThread;
    public static boolean mKohler;
    private ToneGenerator mBeepTone;
    private ArrayAdapter<String> mLogAdapter;
    public static MainActivity mMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meter_reading);
        mBeepTone = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        mLogAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        ((ListView) findViewById(R.id.listView1)).setAdapter(mLogAdapter);
    }

    @Override
    public void onDestroy() {
        if (mReadThread != null) {
            mReadThread.interrupt();
            mReadThread = null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void onReadClicked(View view) {
        mLogAdapter.clear();
        view.setEnabled(false);
        mReadThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (mKohler)
                    ReadKohler(mMainActivity.getMobitProbe());
                else
                    ReadOut(mMainActivity.getMobitProbe());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        view.setEnabled(true);
                    }
                });
            }
        });
        mReadThread.start();
    }

    private void ReadOut(MobitProbe mobitProbe) {
        try {
            mobitProbe.setEnabled(true);
            try {
                mobitProbe.setBaudRate(0);

                IEC62056 iec = new IEC62056(mobitProbe);
                iec.resetIrInputStream();
                iec.writeRequest(null);
                IEC62056.Identification ident = iec.readIdentification();

                addLog(String.format("ManifacturerId = %s, Identification = %s",
                        ident.mnfId, ident.devId));

                /* Elektromed meters need a delay of about 200ms before the option select message */
                if (ident.mnfId.equals("ELM"))
                    Thread.sleep(200);

                int baudId = ident.baudId;

                /* Luna meters can not be read above 9600 bps */
                if (ident.mnfId.equals("LUN") && baudId > 4)
                    baudId = 4;

                iec.writeOptionSelect(0, baudId, 0);
                mobitProbe.setBaudRate(baudId);

                iec.readDataMessageList(this, true);
                mBeepTone.startTone(ToneGenerator.TONE_PROP_ACK, 200);
            } finally {
                mobitProbe.setEnabled(false);
            }
        } catch (Exception e) {
            addLog(e.toString());
        }
    }

    private void ReadKohler(MobitProbe mobitProbe) {
        try {
            mobitProbe.setEnabled(true);
            IEC62056 iec = new IEC62056(mobitProbe);
            try {
                mobitProbe.setBaudRate(0);

                iec.resetIrInputStream();
                iec.writeRequest(null);
                IEC62056.Identification ident = iec.readIdentification();

                addLog(String.format("ManifacturerId = %s, Identification = %s",
                        ident.mnfId, ident.devId));

                iec.writeOptionSelect(0, ident.baudId, 1);
                iec.readAcknowledgement();
                mobitProbe.setBaudRate(ident.baudId);

                addLog(iec.queryDataBlock('2', "0.0.0()"));
                addLog(iec.queryDataBlock('2', "1.8.0()"));
                addLog(iec.queryDataBlock('2', "1.8.1()"));
                addLog(iec.queryDataBlock('2', "1.8.2()"));
                addLog(iec.queryDataBlock('2', "1.8.3()"));

                mBeepTone.startTone(ToneGenerator.TONE_PROP_ACK, 200);
            } finally {
                for (int i = 0; i < 5; i++) {
                    iec.writeBreak();
                    Thread.sleep(100);
                }
                mobitProbe.setEnabled(false);
            }
        } catch (Exception e) {
            addLog(e.toString());
        }
    }

    public void addLog(String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLogAdapter.add(text);
            }
        });
    }
}
