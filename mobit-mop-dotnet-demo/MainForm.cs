﻿using System;
using System.IO;
using System.Media;
using InTheHand.Net;
using System.Threading;
using System.Collections;
using System.Net.Sockets;
using System.Windows.Forms;
using System.ComponentModel;
using InTheHand.Net.Sockets;
using InTheHand.Net.Bluetooth;
using System.Collections.Generic;
using InTheHand.Net.Bluetooth.Factory;

namespace mobit_mop_dotnet_demo
{
	public partial class MainForm : Form
	{
		BluetoothClient m_CliIr, m_CliAt;
		Thread m_AtCommandListenerThread;
		Queue<string> m_AtCmdResponseQueue = new Queue<string>();
		AutoResetEvent m_AtCmdResponseEvent = new AutoResetEvent(false);
		
		const int BLUETOOTH_TIMEDOUT = 5000;

		public MainForm()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			FillDeviceComboBox();
			UpdateControlEnable();
		}

		private void MainForm_Closing(object sender, CancelEventArgs e)
		{
			Disconnect();
		}

		private void buttonConnect_Click(object sender, EventArgs e)
		{
			try
			{
				Connect(((DeviceComboBoxItem)deviceComboBox.SelectedItem).DevInfo.DeviceAddress);
				UpdateControlEnable();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void buttonDisconnect_Click(object sender, EventArgs e)
		{
			Disconnect();
			UpdateControlEnable();
		}

		private void buttonLifePercent_Click(object sender, EventArgs e)
		{
			try
			{
				MessageBox.Show(String.Format("Life percent = %{0}", GetBatteryLifePercent()));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void buttonReadout_Click(object sender, EventArgs e)
		{
			MeterReadingForm form = new MeterReadingForm(m_CliIr.GetStream(), false);
			form.ShowDialog();
		}

		private void buttonKohler_Click(object sender, EventArgs e)
		{
			MeterReadingForm form = new MeterReadingForm(m_CliIr.GetStream(), true);
			form.ShowDialog();
		}

		void Connect(BluetoothAddress devAddr)
		{
			try
			{
				DeviceComboBoxItem item = (DeviceComboBoxItem)deviceComboBox.SelectedItem;

				/* Connect to infrared data channel (channel 1) */
				m_CliIr = new BluetoothClient();
				m_CliIr.Connect(new BluetoothEndPoint(item.DevInfo.DeviceAddress, BluetoothService.Empty, 1));

				/* Connect to AT command channel (channel 2) */
				m_CliAt = new BluetoothClient();
				m_CliAt.Connect(new BluetoothEndPoint(item.DevInfo.DeviceAddress, BluetoothService.Empty, 2));

				/* Start AtCommandListener */
				m_AtCommandListenerThread = new Thread(new ThreadStart(AtCommandListener));
				m_AtCommandListenerThread.Start();
			}
			catch (Exception ex)
			{
				m_CliIr = null;
				m_CliAt = null;
				throw ex;
			}
		}

		void Disconnect()
		{
			if (m_CliIr == null)
				return;

			m_CliIr.Dispose();
			m_CliAt.Dispose();
			m_AtCommandListenerThread.Join();

			m_CliIr = null;
			m_CliAt = null;
			m_AtCommandListenerThread = null;
		}

		int GetBatteryLifePercent()
		{
			/* Post command */
			PostAtCommand("AT+CBC");

			/* Check echo */
			if (!ReadAtCmdResponseLine().Equals("AT+CBC"))
				throw new InvalidDataException();

			/* Skip empty new line */
			ReadAtCmdResponseLine();

			String[] tokens = ReadAtCmdResponseLine().Split(new char[] { ':', ',' });
			if (tokens.Length != 5 || !tokens[0].Equals("+CBC"))
				throw new InvalidDataException();

			return (int.Parse(tokens[2]));
		}

		void UpdateControlEnable()
		{
			if (m_CliIr == null || m_CliAt == null)
			{
				deviceComboBox.Enabled = true;
				buttonConnect.Enabled = true;
				buttonDisconnect.Enabled = false;
				buttonLifePercent.Enabled = false;
				buttonReadout.Enabled = false;
				buttonKohler.Enabled = false;
			}
			else
			{
				deviceComboBox.Enabled = false;
				buttonConnect.Enabled = false;
				buttonDisconnect.Enabled = true;
				buttonLifePercent.Enabled = true;
				buttonReadout.Enabled = true;
				buttonKohler.Enabled = true;
			}
		}

		void FillDeviceComboBox()
		{
			BluetoothClient cli = new BluetoothClient();
			BluetoothDeviceInfo[] peers = cli.DiscoverDevices(255, true, true, false, false);
			foreach (BluetoothDeviceInfo devInfo in peers)
			{
				if (devInfo.DeviceName.StartsWith("MOP-"))
					deviceComboBox.Items.Add(new DeviceComboBoxItem(devInfo));
			}
			deviceComboBox.SelectedIndex = 0;
		}

		void OnTriggerButtonPressed(int buttonId)
		{
			SystemSounds.Beep.Play();
		}

		void PostAtCommand(String command)
		{
			/* Clear old lines */
			m_AtCmdResponseQueue.Clear();
			m_AtCmdResponseEvent.Reset();

			StreamWriter writer = new StreamWriter(m_CliAt.GetStream());
			writer.WriteLine(command);
			writer.Flush();
		}

		String ReadAtCmdResponseLine()
		{
			if (m_AtCmdResponseQueue.Count == 0)
			{
				if (!m_AtCmdResponseEvent.WaitOne(BLUETOOTH_TIMEDOUT, false))
					throw new TimeoutException();
			}

			lock (m_AtCmdResponseQueue)
			{
				return (m_AtCmdResponseQueue.Dequeue());
			}
		}

		void AtCommandListener()
		{
			try
			{
				StreamReader reader = new StreamReader(m_CliAt.GetStream());
				while (m_CliAt.Connected)
				{
					String line = reader.ReadLine();
					if (line.StartsWith("+BTN:"))
					{
						OnTriggerButtonPressed(int.Parse(line.Substring(5)));
					}
					else
					{
						lock (m_AtCmdResponseQueue)
						{
							m_AtCmdResponseQueue.Enqueue(line);
							m_AtCmdResponseEvent.Set();
						}
					}
				}
			}
			catch (Exception /*ex*/)
			{
			}
		}
	}

	class DeviceComboBoxItem : object
	{
		readonly BluetoothDeviceInfo devInfo;

		public DeviceComboBoxItem(BluetoothDeviceInfo devInfo)
		{
			this.devInfo = devInfo;
		}

		public BluetoothDeviceInfo DevInfo
		{
			get
			{
				return this.devInfo;
			}
		}

		public override string ToString()
		{
			return devInfo.DeviceName;
		}
	}
}