﻿/* -------------------------------------------------------------------- */
/* Bluetooth Optical Probe Library										*/
/* -------------------------------------------------------------------- */
/* Developed by "Mobit Bilisim Elektronik Ve Kontrol Sistemleri Dis		*/
/* Tic. A.S.". Please read license.txt for the information and legal	*/
/* notices.																*/
/* -------------------------------------------------------------------- */

using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MobitBtProbeExp
{
	public unsafe static class MobitBtProbe
	{
		public enum MBTBTPROBE_IRBAUDRATE
		{
			MBTBTPROBE_IRBR300,
			MBTBTPROBE_IRBR600,
			MBTBTPROBE_IRBR1200,
			MBTBTPROBE_IRBR2400,
			MBTBTPROBE_IRBR4800,
			MBTBTPROBE_IRBR9600,
			MBTBTPROBE_IRBR19200
		};

#if (PocketPC)
		public const UnmanagedType TCharPtr = UnmanagedType.LPWStr;
#else
		public const UnmanagedType TCharPtr = UnmanagedType.LPStr;
#endif

		[StructLayout(LayoutKind.Sequential)]
		public struct MbtProbePowerStatus
		{
			public Int32 charging;
			public UInt32 batteryVoltage;
			public UInt32 batteryLifePercent;
		};

		public delegate void MbtBtProbeEvent(IntPtr pContext);

		// Optical probe functions
		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern IntPtr CreateMbtBtProbe([MarshalAs(TCharPtr)] String pDevice);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern void DeleteMbtBtProbe(IntPtr hBtProbe);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern void MbtBtProbe_SetPowerEventProc(IntPtr hBtProbe,
			MbtBtProbeEvent pPowerEventProc, IntPtr pContext);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern void MbtBtProbe_SetTriggerEventProc(IntPtr hBtProbe,
			MbtBtProbeEvent pTriggerEventProc, IntPtr pContext);

		// Infrared stream functions
		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_Read(IntPtr hBtProbe, IntPtr pBuffer,
			UInt32 bytesToRead, out UInt32 pBytesRead);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_Write(IntPtr hBtProbe, IntPtr pBuffer,
			UInt32 bytesToWrite, out UInt32 pBytesWritten);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_SetTimeouts(IntPtr hBtProbe, UInt32 readTimeout, 
			UInt32 writeTimeout);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_FlushWrite(IntPtr hBtProbe);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_CleanRead(IntPtr hBtProbe);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_AbortRead(IntPtr hBtProbe);

		// Probe commands
		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_GetVersion(IntPtr hBtProbe, out UInt16 pVersion);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_GetBtVersion(IntPtr hBtProbe, IntPtr pVersion, UInt32 maxLen);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_PowerOff(IntPtr hBtProbe);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_SetLed(IntPtr hBtProbe, bool enable);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_GetPowerStatus(IntPtr hBtProbe, out MbtProbePowerStatus pStatus);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_SetAutoPowerOffTime(IntPtr hBtProbe, UInt16 duration);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_IrEnable(IntPtr hBtProbe, bool enable);

		[DllImport("mobitbtprobe.dll", SetLastError = true)]
		public static extern bool MbtBtProbe_IrSetBaudRate(IntPtr hBtProbe, MBTBTPROBE_IRBAUDRATE baudRate);

// Utility functions
		public static bool MbtBtProbe_ReadByte(IntPtr hBtProbe, ref byte Byte)
		{
			UInt32 bytesRead;

			fixed (byte* pByte = &Byte)
			{
				if (!MobitBtProbe.MbtBtProbe_Read(hBtProbe, (IntPtr)pByte, 1, out bytesRead))
					return (false);
			}
			return (bytesRead == 1);
		}

		public static bool MbtBtProbe_ReadUntil(IntPtr hBtProbe, ref byte[] byteArray,
			byte stop1, byte stop2, byte stop3, ref byte stop)
		{
			byte value = 0;
			List<byte> byteList = new List<byte>();

			while (true)
			{
				if (!MbtBtProbe_ReadByte(hBtProbe, ref value))
					return (false);

				if (value == stop1 || value == stop2 || value == stop3)
					break;

				byteList.Add(value);
			}

			byteArray = byteList.ToArray();
			stop = value;
			return (true);
		}

		public static bool MbtBtProbe_WriteAll(IntPtr hBtProbe, byte[] buffer)
		{
			UInt32 totalWritten;

			totalWritten = 0;
			unsafe
			{
				fixed (byte* pUnsafeBuffer = buffer)
				{
					while (totalWritten < buffer.Length)
					{
						UInt32 bytesWritten;

						if (!MobitBtProbe.MbtBtProbe_Write(hBtProbe, (IntPtr)(pUnsafeBuffer + totalWritten),
							(UInt32)(buffer.Length - totalWritten), out bytesWritten))
							return (false);

						if (bytesWritten == 0)
							break;

						totalWritten += bytesWritten;
					}
				}
			}
			return (totalWritten == buffer.Length);
		}
	}
}
