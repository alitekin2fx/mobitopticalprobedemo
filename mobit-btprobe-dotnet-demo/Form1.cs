﻿using System;
using System.Text;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using MobitBtProbeExp;

namespace MobitOptReadDemo
{
	public unsafe partial class Form1 : Form
	{
		IntPtr m_hOptPrb;
		object m_OptPrbLock;
		GCHandle m_GCHandle;
		Thread m_RunTestsThread;

		const int STX = 0x02;
		const int ETX = 0x03;
		const int ACK = 0x06;
		delegate void DataLineHandlerDelegate(string dataLine);

		MobitBtProbe.MbtBtProbeEvent m_PowerEventProcPtr;
		MobitBtProbe.MbtBtProbeEvent m_TriggerEventProcPtr;

		delegate void AddLogCallback(string text);
		delegate void ClearIListCallback(IList list);
		delegate void AddListItemCallback(string code, string value);
		delegate void SetControlTextCallback(Control control, string text);
		delegate string GetControlTextCallback(Control control);
		delegate void SetControlEnabledCallback(Control control, bool enabled);

		public Form1()
		{
			InitializeComponent();

			m_hOptPrb = IntPtr.Zero;
			m_OptPrbLock = new object();
			m_GCHandle = GCHandle.Alloc(this);
			m_PowerEventProcPtr = new MobitBtProbe.MbtBtProbeEvent(PowerEventProc);
			m_TriggerEventProcPtr = new MobitBtProbe.MbtBtProbeEvent(TriggerEventProc);
			ComPort.Text = "COM6:";
		}

		~Form1()
		{
			m_GCHandle.Free();
		}

		void WriteRequestMessage(string devAddr)
		{
			MobitBtProbe.MbtBtProbe_WriteAll(m_hOptPrb, new byte[] { (byte)'/', (byte)'?' });
			MobitBtProbe.MbtBtProbe_WriteAll(m_hOptPrb, new ASCIIEncoding().GetBytes(devAddr));
			MobitBtProbe.MbtBtProbe_WriteAll(m_hOptPrb, new byte[] { (byte)'!', (byte)'\r', (byte)'\n' });
			MobitBtProbe.MbtBtProbe_FlushWrite(m_hOptPrb);
		}

		bool ReadIdentificationMessage(ref string mnfId, ref string identification,
			ref int baudRateId)
		{
			byte[] msgLine = null;
			byte chr1 = 0, chr2 = 0;

			if (!MobitBtProbe.MbtBtProbe_ReadUntil(m_hOptPrb, ref msgLine, (byte)'\r',
				(byte)'\n', 0, ref chr1))
				return (false);

			if (!MobitBtProbe.MbtBtProbe_ReadByte(m_hOptPrb, ref chr2))
				return (false);

			if ((chr1 != '\r' || chr2 != '\n') && (chr1 != '\n' || chr2 != '\r'))
				return (false);

			if (msgLine.Length < (1 + 3 + 1))
				return (false);

			if (msgLine[0] != '/')
				return (false);

			identification = Encoding.ASCII.GetString(msgLine, 5, msgLine.Length - 5);
			mnfId = Encoding.ASCII.GetString(msgLine, 1, 3);
			baudRateId = msgLine[4] - '0';
			return (true);
		}

		void WriteOptionSelectMessage(int protocol, int baudRateId, int mode)
		{
			byte[] msg = new byte[] { (byte)ACK, (byte)('0' + protocol), (byte)('0' + baudRateId), 
				(byte)('0' + mode), (byte)'\r', (byte)'\n' };
			MobitBtProbe.MbtBtProbe_WriteAll(m_hOptPrb, msg);
			MobitBtProbe.MbtBtProbe_FlushWrite(m_hOptPrb);
		}

		bool ReadDataMessage(DataLineHandlerDelegate dataLineProc)
		{
			byte[] dataLine = null;
			byte chr1 = 0, chr2 = 0, bcc = 0;

			if (!MobitBtProbe.MbtBtProbe_ReadByte(m_hOptPrb, ref chr1) || chr1 != STX)
				return (false);

			while(true)
			{
				if (!MobitBtProbe.MbtBtProbe_ReadUntil(m_hOptPrb, ref dataLine, (byte)'\r',
					(byte)'\n', ETX, ref chr1))
					return (false);

				bcc = CalculateBCC(bcc, dataLine);
				bcc = CalculateBCC(bcc, chr1);

				if (chr1 == ETX)
					break;

				if (!MobitBtProbe.MbtBtProbe_ReadByte(m_hOptPrb, ref chr2))
					return (false);

				bcc = CalculateBCC(bcc, chr2);

				if ((chr1 != '\r' || chr2 != '\n') && (chr1 != '\n' || chr2 != '\r'))
					return (false);

				dataLineProc(Encoding.ASCII.GetString(dataLine, 0, dataLine.Length));
			}

			if (!MobitBtProbe.MbtBtProbe_ReadByte(m_hOptPrb, ref chr1))
				return (false);

			return (chr1 == bcc);
		}

		bool SetBaudRate(int baudRateId)
		{
			MobitBtProbe.MBTBTPROBE_IRBAUDRATE baudRate;

			switch (baudRateId)
			{
			case 0: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR300; break;
			case 1: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR600; break;
			case 2: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR1200; break;
			case 3: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR2400; break;
			case 4: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR4800; break;
			case 5: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR9600; break;
			case 6: baudRate = MobitBtProbe.MBTBTPROBE_IRBAUDRATE.MBTBTPROBE_IRBR19200; break;
			default: return(false);
			}
			return (MobitBtProbe.MbtBtProbe_IrSetBaudRate(m_hOptPrb, baudRate));
		}

		byte CalculateBCC(byte prevBcc, byte data)
		{
			return ((byte)(prevBcc ^ data));
		}

		byte CalculateBCC(byte prevBcc, byte[] data)
		{
			byte bcc;

			bcc = prevBcc;
			for (int index = 0; index < data.Length; index++)
				bcc = CalculateBCC(bcc, data[index]);

			return (bcc);
		}

		bool ReadByReadOut(DataLineHandlerDelegate dataLineProc)
		{
			int baudRateId = 0;
			string mnfId = "", identification = "";

			if (!SetBaudRate(0))
				return(false);

			WriteRequestMessage("");
			if (!ReadIdentificationMessage(ref mnfId, ref identification, ref baudRateId))
				return(false);

			WriteOptionSelectMessage(0, baudRateId, 0);
			if (!SetBaudRate(baudRateId))
				return (false);

			return (ReadDataMessage(dataLineProc));
		}

		private void ConnectionThreadProc()
		{
			try
			{
				string DeviceName = (string)Invoke(new GetControlTextCallback(GetControlText), 
					new object[] { ComPort });

				Invoke(new AddLogCallback(AddLog), String.Format("Opening device '{0}' ...", DeviceName));

				IntPtr hOptPrb = MobitBtProbe.CreateMbtBtProbe(DeviceName);
				if (hOptPrb == IntPtr.Zero)
				{
					Invoke(new AddLogCallback(AddLog), "Failed to create probe handle.");
					return;
				}

				MobitBtProbe.MbtBtProbe_SetPowerEventProc(hOptPrb, m_PowerEventProcPtr, (IntPtr)m_GCHandle);
				MobitBtProbe.MbtBtProbe_SetTriggerEventProc(hOptPrb, m_TriggerEventProcPtr, (IntPtr)m_GCHandle);

				Invoke(new SetControlTextCallback(SetControlText), new object[] { ToggleConnectBtn, 
					"Disconnect" });
				Invoke(new SetControlEnabledCallback(SetControlEnabled), new object[] { RunTests, true });
				Invoke(new AddLogCallback(AddLog), "Done.");

				m_hOptPrb = hOptPrb;
// Update device power status
				Thread thread = new Thread(new ThreadStart(UpdatePowerStatusThreadProc));
				thread.Start();
			}
			finally
			{
				Invoke(new SetControlEnabledCallback(SetControlEnabled), new object[] { ToggleConnectBtn, true });
				Invoke(new SetControlEnabledCallback(SetControlEnabled), new object[] { CloseBtn, true });
			}
		}

		void DataLineHandler(string dataLine)
		{
			Invoke(new AddLogCallback(AddLog), dataLine);
		}

		private void RunTestsThreadProc()
		{
			Monitor.Enter(m_OptPrbLock);
			try
			{
				if (!MobitBtProbe.MbtBtProbe_CleanRead(m_hOptPrb))
					return;

				if (!MobitBtProbe.MbtBtProbe_IrEnable(m_hOptPrb, true))
					return;

				ReadByReadOut(DataLineHandler);

				MobitBtProbe.MbtBtProbe_IrEnable(m_hOptPrb, false);
			}
			finally
			{
				Monitor.Exit(m_OptPrbLock);
				Invoke(new SetControlEnabledCallback(SetControlEnabled), new object[] { ToggleConnectBtn, true });
				Invoke(new SetControlEnabledCallback(SetControlEnabled), new object[] { RunTests, true });
				Invoke(new SetControlEnabledCallback(SetControlEnabled), new object[] { CloseBtn, true });
				Invoke(new AddLogCallback(AddLog), "Done.");
				m_RunTestsThread = null;
			}
		}

		private void UpdatePowerStatusThreadProc()
		{
			MobitBtProbe.MbtProbePowerStatus status;

			Monitor.Enter(m_OptPrbLock);
			try
			{
				if (!MobitBtProbe.MbtBtProbe_GetPowerStatus(m_hOptPrb, out status))
					return;
			}
			finally
			{
				Monitor.Exit(m_OptPrbLock);
			}

			if (status.batteryLifePercent == 255)
				return;

			if (status.charging != 0)
			{
				Invoke(new SetControlTextCallback(SetControlText), new object[] { BatteryLevel, 
					String.Format("Charging",  status.batteryLifePercent)});
			}
			else
			{
				Invoke(new SetControlTextCallback(SetControlText), new object[] { BatteryLevel, 
					String.Format("%{0}",  status.batteryLifePercent.ToString())});
			}
		}

		private static void TriggerEventProc(IntPtr pContext)
		{
			Form1 thisForm = (Form1)(((GCHandle)pContext).Target);
			if (thisForm.m_RunTestsThread != null)
				return;

			thisForm.Invoke(new SetControlEnabledCallback(thisForm.SetControlEnabled), new object[] { 
				thisForm.ToggleConnectBtn, false });

			thisForm.Invoke(new SetControlEnabledCallback(thisForm.SetControlEnabled), new object[] { 
				thisForm.RunTests, false });

			thisForm.Invoke(new SetControlEnabledCallback(thisForm.SetControlEnabled), new object[] { 
				thisForm.CloseBtn, false });

			thisForm.Invoke(new ClearIListCallback(thisForm.ClearIList), thisForm.LogListBox.Items);

			thisForm.m_RunTestsThread = new Thread(new ThreadStart(thisForm.RunTestsThreadProc));
			thisForm.m_RunTestsThread.Start();
		}

		private static void PowerEventProc(IntPtr pContext)
		{
			Form1 thisForm = (Form1)(((GCHandle)pContext).Target);
			Thread thread = new Thread(new ThreadStart(thisForm.UpdatePowerStatusThreadProc));
			thread.Start();
		}

		private void AddLog(string text)
		{
			LogListBox.SelectedIndex = LogListBox.Items.Add(text);
		}

		private void SetControlText(Control control, string text)
		{
			control.Text = text;
		}

		public string GetControlText(Control control)
		{
			return (control.Text);
		}

		private void SetControlEnabled(Control control, bool enabled)
		{
			control.Enabled = enabled;
		}

		private void ClearIList(IList list)
		{
			list.Clear();
		}

		private void ToggleConnectBtn_Click(object sender, EventArgs e)
		{
			if (m_hOptPrb == IntPtr.Zero)
			{
				CloseBtn.Enabled = false;
				ToggleConnectBtn.Enabled = false;

				Thread thread = new Thread(new ThreadStart(ConnectionThreadProc));
				thread.Start();
			}
			else
			{
				MobitBtProbe.DeleteMbtBtProbe(m_hOptPrb);
				m_hOptPrb = IntPtr.Zero;

				ToggleConnectBtn.Text = "Connect";
				RunTests.Enabled = false;
			}
		}

		private void RunTests_Click(object sender, EventArgs e)
		{
			RunTests.Enabled = false;
			CloseBtn.Enabled = false;
			ToggleConnectBtn.Enabled = false;

			LogListBox.Items.Clear();

			m_RunTestsThread = new Thread(new ThreadStart(RunTestsThreadProc));
			m_RunTestsThread.Start();
		}

		private void CloseBtn_Click(object sender, EventArgs e)
		{
			// Form kapatilmadan once pOptPrb ve hOptRead nesnelerini silmek 
			// gerekiyor !.
			if (!ToggleConnectBtn.Enabled || m_hOptPrb != IntPtr.Zero)
			{
				MessageBox.Show("You need to disconnect from the device.");
				return;
			}
			Application.Exit();
		}
	}
}
