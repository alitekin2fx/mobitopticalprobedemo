﻿namespace MobitOptReadDemo
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.BatteryLevel = new System.Windows.Forms.Label();
			this.LogListBox = new System.Windows.Forms.ListBox();
			this.ToggleConnectBtn = new System.Windows.Forms.Button();
			this.RunTests = new System.Windows.Forms.Button();
			this.CloseBtn = new System.Windows.Forms.Button();
			this.ComPort = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(3, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(109, 15);
			this.label1.Text = "Battery Level :";
			// 
			// BatteryLevel
			// 
			this.BatteryLevel.Location = new System.Drawing.Point(161, 32);
			this.BatteryLevel.Name = "BatteryLevel";
			this.BatteryLevel.Size = new System.Drawing.Size(76, 15);
			this.BatteryLevel.Text = "XXX";
			// 
			// LogListBox
			// 
			this.LogListBox.Location = new System.Drawing.Point(3, 53);
			this.LogListBox.Name = "LogListBox";
			this.LogListBox.Size = new System.Drawing.Size(234, 170);
			this.LogListBox.TabIndex = 1;
			// 
			// ToggleConnectBtn
			// 
			this.ToggleConnectBtn.Location = new System.Drawing.Point(5, 239);
			this.ToggleConnectBtn.Name = "ToggleConnectBtn";
			this.ToggleConnectBtn.Size = new System.Drawing.Size(73, 23);
			this.ToggleConnectBtn.TabIndex = 2;
			this.ToggleConnectBtn.Text = "Connect";
			this.ToggleConnectBtn.Click += new System.EventHandler(this.ToggleConnectBtn_Click);
			// 
			// RunTests
			// 
			this.RunTests.Enabled = false;
			this.RunTests.Location = new System.Drawing.Point(83, 239);
			this.RunTests.Name = "RunTests";
			this.RunTests.Size = new System.Drawing.Size(73, 23);
			this.RunTests.TabIndex = 3;
			this.RunTests.Text = "Run Tests";
			this.RunTests.Click += new System.EventHandler(this.RunTests_Click);
			// 
			// CloseBtn
			// 
			this.CloseBtn.Location = new System.Drawing.Point(161, 239);
			this.CloseBtn.Name = "CloseBtn";
			this.CloseBtn.Size = new System.Drawing.Size(73, 23);
			this.CloseBtn.TabIndex = 4;
			this.CloseBtn.Text = "Close";
			this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
			// 
			// ComPort
			// 
			this.ComPort.Location = new System.Drawing.Point(161, 3);
			this.ComPort.Name = "ComPort";
			this.ComPort.Size = new System.Drawing.Size(76, 21);
			this.ComPort.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(3, 4);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 20);
			this.label2.Text = "Com Port :";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.ClientSize = new System.Drawing.Size(240, 268);
			this.ControlBox = false;
			this.Controls.Add(this.label2);
			this.Controls.Add(this.ComPort);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.BatteryLevel);
			this.Controls.Add(this.LogListBox);
			this.Controls.Add(this.ToggleConnectBtn);
			this.Controls.Add(this.RunTests);
			this.Controls.Add(this.CloseBtn);
			this.KeyPreview = true;
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label BatteryLevel;
		private System.Windows.Forms.ListBox LogListBox;
		private System.Windows.Forms.Button ToggleConnectBtn;
		private System.Windows.Forms.Button RunTests;
		private System.Windows.Forms.Button CloseBtn;
		private System.Windows.Forms.TextBox ComPort;
		private System.Windows.Forms.Label label2;
	}
}

