﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;

namespace MobitOptReadDemo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
		[MTAThread]
		static void Main()
        {
			Application.Run(new Form1());
		}
    }
}
