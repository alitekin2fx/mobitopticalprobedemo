﻿namespace mobit_mop_dotnet_demo
{
	partial class MeterReadingForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MainMenu mainMenu1;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.obisListBox = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonRead = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// obisListBox
			// 
			this.obisListBox.Location = new System.Drawing.Point(4, 32);
			this.obisListBox.Name = "obisListBox";
			this.obisListBox.Size = new System.Drawing.Size(233, 198);
			this.obisListBox.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.BackColor = System.Drawing.Color.Blue;
			this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
			this.label1.ForeColor = System.Drawing.SystemColors.Window;
			this.label1.Location = new System.Drawing.Point(3, 4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(234, 20);
			this.label1.Text = "Obis List";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// buttonRead
			// 
			this.buttonRead.Location = new System.Drawing.Point(84, 245);
			this.buttonRead.Name = "buttonRead";
			this.buttonRead.Size = new System.Drawing.Size(72, 20);
			this.buttonRead.TabIndex = 3;
			this.buttonRead.Text = "Read";
			this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
			// 
			// MeterReadingForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(240, 268);
			this.Controls.Add(this.buttonRead);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.obisListBox);
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "MeterReadingForm";
			this.Text = "MeterReadingForm";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox obisListBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonRead;
	}
}